<?php

namespace controller;

class AccountController {
    static function cartAdd(){
        //var_dump($_POST);
        session_start();
        for($i=0;$i<sizeof($_SESSION["cart"]);$i++){
            if($_SESSION["cart"][$i]["P_id"]==$_POST["P_id"]){
                $_SESSION["cart"][$i]["NoO"]++;
                header("Location: /cart");
                exit();
            }
        }
        if($_SESSION["cart"]!=null){
            $_SESSION["cart"][sizeof($_SESSION["cart"])]=$_POST;
            var_dump($_SESSION);
        }else{
            $_SESSION["cart"] [0]=   $_POST;
        }
        header("Location: /cart");
    }

    static function cart(){
        session_start();
        $products=\model\AccountModel::cart_show($_SESSION["cart"]);
        $params = [
            "title"  => "cart",
            "module" => "cart.php",
            "products" => $products
        ];
        \view\Template::render($params);
    }

    static function update(){
        session_start();
        var_dump($_SESSION);
        $Infos = $_POST;
        if(strlen($Infos["LastName"])<=2 or strlen($Infos["firstName"])<=2 or filter_var($Infos["Email"],FILTER_VALIDATE_EMAIL)==false){
            header("Location: /account/infos?status=error");
            exit();
        }else{
            \model\AccountModel::update($Infos["LastName"],$Infos["firstName"],$Infos["Email"],$_SESSION["id"]);
            $_SESSION["firstname"]=$Infos["firstName"];
            $_SESSION["lastname"]=$Infos["LastName"];
            $_SESSION["mail"]=$Infos["Email"];
            header("Location: /account/infos?status=reussi");
        }

    }

    static function infos(){
        //echo "404 Not Found!";
        $params = [
            "title"  => "infos",
            "module" => "infos.php"
        ];
        \view\Template::render($params);
    }

    public function account(): void
    {
        //echo "404 Not Found!";
        $params = [
            "title"  => "Account",
            "module" => "account.php"
        ];
        \view\Template::render($params);
    }

    public function signin(){
        $Infos = $_POST;
        $res=\model\AccountModel::signin($Infos["userlastname"],$Infos["userfirstname"],$Infos["usermail"],password_hash($Infos["userpass"],PASSWORD_DEFAULT));

        if($res==true){
            header("Location: /account?status=signin_success");
            exit();
        }else{
            header("Location: /account?status=signin_fail");
            exit();
        }

    }

    public function login(){
        $Infos = $_POST;
        //var_dump($Infos);
        $serRep = \model\AccountModel::login($Infos["usermail"],$Infos["userpass"]);
        var_dump($serRep);
        if($serRep!=null){
            session_start();
            $_SESSION=$serRep;
            header("Location: /store");
            exit();
        }

        header("Location: /account?status=login_fail");
        exit();
    }

    public function logout(){
        session_start();
        session_destroy();
        header("Location: /account?status=logout_success");
        exit();
    }

}