
<div id="account">
    <?php
    if($_GET!=null){
        if($_GET['status']=='error'){
            echo "<p  class='error box'>Error of updating</p>";
        }elseif ($_GET['status']=='reussi'){
            echo "<p  class='valid box'>Update Done!</p>";
        }
    }
    ?>
    <h2>Informations du compte</h2>
    <h3>Informations personnelles</h3>
    <?php if($_SESSION!=null){?>
    <form method="post" action="/account/update">
        <p>Prénom</p>
        <input type="text" name="firstName" placeholder="<?php echo $_SESSION["firstname"]?>">
        <p>Nom</p>
        <input type="text" name="LastName" placeholder="<?php echo $_SESSION["lastname"]?>">
        <p>Adresse mail</p>
        <input type="text" name="Email" placeholder="<?php echo $_SESSION["mail"]?>">

        <input type="submit" value="Modifier mes informations">
    </form>
    <?php }else{
        header("Location: /account");
    } ?>
    <h3>Commandes</h3>
    <p>Tu n'as pas de commandes en cours</p>
</div>