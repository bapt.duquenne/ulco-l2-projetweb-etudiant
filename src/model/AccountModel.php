<?php

namespace model;

class AccountModel {
    static function cart_show($cart){
        //var_dump($cart);
        $updatedcart=$cart;
        $i=0;
        foreach ($cart as $c){
            //var_dump($c);
            $db = \model\Model::connect();
            $req =$db->prepare("SELECT product.id,product.name,price ,image , category.name AS category FROM product
	                                INNER JOIN category
    	                                WHERE (product.category=category.id)AND(product.id=".$c["P_id"].");");
            $req->execute();
            //var_dump($req->fetch());
            $updatedcart[$i]["product_infos"]=$req->fetch();
            // Retourner les résultats (type array)
            $i++;
        }
        //var_dump($updatedcart);
        return $updatedcart;
    }

    /**
     * @param $firstname
     * @param $lastname
     * @param $mail
     * @param $accountNO
     * maybe add security?
     */
    static function update($firstname,$lastname,$mail,$accountNO){
        $db = \model\Model::connect();
        $req =$db->prepare("UPDATE account SET firstname = '".$firstname."',
                                                    lastname ='".$lastname."',
                                                    mail ='".$mail."'WHERE id=".$accountNO);
        $req->execute();
    }

    static function check($firstname,$lastname,$mail,$password): bool{
        // Connexion à la base de données
        $db = \model\Model::connect();

        $req =$db->prepare("SELECT COUNT(*) as count FROM account WHERE account.mail LIKE '".$mail."'");
        $req->execute();
        $mails=$req->fetch();
        //var_dump($mails);
        //ai pas testé l'email !!
        if(strlen($firstname)<2){
            echo "fn trop court";
            return false;
        }
        if(strlen($lastname)<2){
            echo "ln trop court";
            return false;
        }
        if(filter_var($mail,FILTER_VALIDATE_EMAIL)==false){
            echo $mail." : Mauvais email";
            return false;
        }
        //echo $mails['count'];

        if($mails['count']>0){
            echo $mail." : mail répété ! ";
            return false;
        }



        if(strlen($password)<6){
            echo "ps trop court";
            return false;
        }
        return true;
    }

    static function signin($firstname,$lastname,$mail,$password): bool{

        if(self::check($firstname,$lastname,$mail,$password)){
            $db = \model\Model::connect();
            $que="INSERT INTO account VALUES (0,'".$firstname."','".$lastname."','".$mail."','".$password."');";
            echo $firstname," ",$lastname," ",$mail," ",$password;
            $req =$db->prepare($que);
            $req->execute();
            return true;
        }
        return false;
    }

    static function login($mail,$password){
        $db = \model\Model::connect();
        //password must be encrypted
        $que="SELECT * FROM account WHERE(mail = '".$mail."')";
        $req =$db->prepare($que);
        $req->execute();
        $valls = $req->fetchAll();
        //var_dump($valls);
        for($j=0;$j<sizeof($valls);$j++){
            if(password_verify($password,$valls[$j]["password"])==true){
                return $valls[$j];
            }
        }
        return null;
    }

}