

<?php //var_dump($_GET["status"]);
    if($_GET!=null) {
        if ($_GET["status"] == "login_fail") {
            echo '<p  class="error box">La connexion a echoué. verifiez vois identifiants et réesayer !</p>';
        } else if ($_GET["status"] == "logout_success") {
            echo '<p  class="valid box">Deconnection réussie !!</p>';
        } else if ($_GET["status"] == "signin_success") {
            echo '<p  class="valid box">Creation du compte réussie !!</p>';
        } else if ($_GET["status"] == "signin_fail") {
            echo '<p  class="error box">Erreur de création du compte</p>';
        }
    }
?>

<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p class="invalid" id="NameV">Nom</p>
  <input type="text" id="lm" class="invalid" name="userlastname" placeholder="Nom" onchange="updateName()"/>

  <p class="invalid" id="PrenameV">Prénom</p>
  <input type="text" id="fn" class="invalid" name="userfirstname" placeholder="Prénom" onchange="updatePname()"/>

  <p class="invalid" id="MailV">Adresse mail</p>
  <input type="text" id="mail" class="invalid" name="usermail" placeholder="Adresse mail" onchange="updateMail()"/>

  <p class="invalid" id="PassV">Mot de passe</p>
  <input type="password" id="pass"  class="invalid"name="userpass" placeholder="Mot de passe" onchange="updatePass1()"/>

  <p class="invalid" id="RePassV">Répéter le mot de passe</p>
  <input type="password" id="re_pass" class="invalid" name="re-userpass" placeholder="Mot de passe" onchange="updatePass2()"/>

  <input type="submit" id="fs" value="Inscription" disabled/>

</form>

</div>

<script src="/public/scripts/signin.js"></script>






















