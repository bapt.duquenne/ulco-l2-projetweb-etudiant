submitButton=document.getElementById("fs");

function updateName(){
    console.log("updating name ...")
    NameText=document.getElementById("lm");
    NameTag=document.getElementById("NameV");
    console.log("updating name ...");
    if(NameText.value.length>=2){
        NameText.classList.add("valid")
        NameText.classList.remove("invalid")
        NameTag.classList.add("valid")
        NameTag.classList.remove("invalid")
    }else if(NameText.value.length<2){
        NameText.classList.add("invalid")
        NameText.classList.remove("valid")
        NameTag.classList.add("invalid")
        NameTag.classList.remove("valid")
    }
    invalids=document.getElementsByClassName("invalid");
    //console.log(invalids.length);
    if(invalids.length==0){
        submitButton.disabled=false;
    }
}

function updateMail(){
    console.log("updating mail ...")
    Mail=document.getElementById("mail");
    MailTag=document.getElementById("MailV");
    verif=/^[a-zA-Z0-9_-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
    console.log(Mail);
    console.log(MailTag);
    if(verif.exec(Mail.value) != null){
        Mail.classList.add("valid")
        Mail.classList.remove("invalid")
        MailTag.classList.add("valid")
        MailTag.classList.remove("invalid")
    }else{
        Mail.classList.add("invalid")
        Mail.classList.remove("valid")
        MailTag.classList.add("invalid")
        MailTag.classList.remove("valid")
    }
    invalids=document.getElementsByClassName("invalid");
    //console.log(invalids.length);
    if(invalids.length==0){
        submitButton.disabled=false;
    }
}

function updatePname(){
    console.log("updating lastname ...")
    PNameText=document.getElementById("fn");
    PNameTag=document.getElementById("PrenameV");
    if(PNameText.value.length>=2){
        PNameText.classList.add("valid")
        PNameText.classList.remove("invalid")
        PNameTag.classList.add("valid")
        PNameTag.classList.remove("invalid")
    }else{
        PNameText.classList.add("invalid")
        PNameText.classList.remove("valid")
        PNameTag.classList.add("invalid")
        PNameTag.classList.remove("valid")
    }
    invalids=document.getElementsByClassName("invalid");
    //console.log(invalids.length);
    if(invalids.length==0){
        submitButton.disabled=false;
    }
}

function updatePass1(){
    console.log("updating password ...")
    PNameText=document.getElementById("pass");
    PNameTag=document.getElementById("PassV");
    verif=/^(?=.*\d)(?=.*[a-z]).{6,}/;
    if(verif.exec(PNameText.value)!=null){
        PNameText.classList.add("valid")
        PNameText.classList.remove("invalid")
        PNameTag.classList.add("valid")
        PNameTag.classList.remove("invalid")
    }else{
        PNameText.classList.add("invalid")
        PNameText.classList.remove("valid")
        PNameTag.classList.add("invalid")
        PNameTag.classList.remove("valid")
    }
    invalids=document.getElementsByClassName("invalid");
    //console.log(invalids.length);
    if(invalids.length==0){
        submitButton.disabled=false;
    }
}

function updatePass2(){
    console.log("updating password ...")
    PNameText=document.getElementById("re_pass");
    PNameTag=document.getElementById("RePassV");
    truepass = document.getElementById("pass");
    if(truepass.value==PNameText.value){
        PNameText.classList.add("valid")
        PNameText.classList.remove("invalid")
        PNameTag.classList.add("valid")
        PNameTag.classList.remove("invalid")
    }else{
        PNameText.classList.add("invalid")
        PNameText.classList.remove("valid")
        PNameTag.classList.add("invalid")
        PNameTag.classList.remove("valid")
    }
    invalids=document.getElementsByClassName("invalid");
    //console.log(invalids.length);
    if(invalids.length==0){
        submitButton.disabled=false;
    }
}



console.log(submitButton);
