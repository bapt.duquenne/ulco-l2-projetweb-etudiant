<?php
$id=$params["id"];
$params=$params["product"];?>
<div id="product">
    <div>
        <div class="product-images">
            <div id="test">
                <img src="/public/images/<?php echo $params["image"]?>"/>
            </div>
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?php echo $params["image"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["image_alt1"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["image_alt2"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["image_alt3"]?>"/>
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"> <?php echo $params["category"]?> </p>
            <h1> <?php echo $params["name"]?></h1>
            <p class="product-price"><?php echo $params["price"]?>€</p>
            <div id="error"></div>
            <form method="post" action="/cart/add">
                <button type="button" class="plus">+</button>
                <button type="button" class="indic">1</button>
                <button type="button"class="moin">-</button>
                <input type="hidden" name="P_id" value="<?php echo $id?>"/>
                <input type="hidden" name="NoO" class="CMV" value="1"/>
                <input type="submit"  >
            </form>
        </div>

    </div>
    <div>
        <div class="product-spec">
            <h2>
                Spécificités
            </h2>
            <?php echo $params["spec"]?>
        </div>
        <div class="product-comments">
            <h2>
                Avis
            </h2>
            <?php
                $comments=\model\CommentModel::listComment($id);
                //var_dump($comments);
                foreach($comments as $comment){
                    ?>
                        <div class="product-comment">
                            <p class="product-comment-author"><?php echo $comment["firstname"]." ".$comment["lastname"]?></p>
                            <p>
                                <?php echo $comment["content"];?>
                            </p>
                        </div>
                    <?php
                }
            ?>
            <form method="post" action="/comment/postComment">
                <input type="text" name="comment" placeholder="Your comment..." />
                <input type="hidden" name="P_id" value="<?php echo $id?>"/>
                <input type="hidden" name="A_id" value="<?php echo $_SESSION["id"]?>"/>
            </form>
        </div>

    </div>
</div>
<script src="/public/scripts/product.js"></script>
