<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts():array
  {

      $db = \model\Model::connect();
      $req =$db->prepare("SELECT product.id,product.name,price ,image , category.name AS category FROM product
	                                INNER JOIN category
    	                                ON(product.category=category.id);");
      $req->execute();

      // Retourner les résultats (type array)
      return $req->fetchAll();
  }

    static function infoProducts(int $id):array{
        $db = \model\Model::connect();
        $que="SELECT product.name,price ,image ,image_alt1,image_alt2,image_alt3, spec, category.name AS category FROM product
	                                INNER JOIN category
    	                                WHERE(product.category=category.id)AND(product.id=".(string)$id.");";
        $req =$db->prepare($que);
        $req->execute();
        $Rvalue=$req->fetch();
        if($Rvalue==null){
            return array();
        }
        return $Rvalue;
    }

    static function search(array $Stuffarray){
        error_reporting(E_ERROR | E_PARSE);
        $db = \model\Model::connect();
        //var_dump($Stuffarray);
        $i=0;
        $que="SELECT product.id,product.name,price ,image , category.name AS category FROM product
	                                INNER JOIN category ";
        if($Stuffarray!=null OR (!($Stuffarray["search"]=="" AND sizeof($Stuffarray)==1))){
            $que = $que."WHERE product.category=category.id ";
            if($Stuffarray["search"]!=""){
                $que = $que."AND (product.name LIKE '%".$Stuffarray["search"]."%')";
                $i++;
            }
            if($Stuffarray["category"]!=null){
                if (sizeof($Stuffarray["category"]) != 0) {
                    $que = $que . " AND ";

                    $que = $que . " (category.name = '" . $Stuffarray["category"][0] . "')";
                    $i++;
                }
            }
            /**
             * pb pour les ordres ?
             */
            if($Stuffarray["order"]!=null) {
                if ($Stuffarray["order"] == 'on') {
                    $que = $que . " ORDER BY product.name";
                }
            }
        }
        //echo $que;
        $req =$db->prepare($que);
        $req->execute();
        $Rvalue=$req->fetchAll();
        //var_dump($Rvalue);
        return $Rvalue;
    }

}