<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" action="/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
  <input type="radio" name="order" /> Croissant <br />
  <input type="radio" name="order" /> Décroissant <br />

  <div><input type="submit" value="Appliquer" /></div>


</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

<!-- TODO: Afficher la liste des produits ici -->
<?php foreach ($params["products"] as $c) { ?>
    <div class="card">
        <p class="card-image">
            <img src="/public/images/<?php echo $c["image"] ?>"/>
        </p>
        <p class="card-category">
            <?php echo $c["category"] ?>
        </p>
        <p class="card-title">
            <a href="/store/<?php echo $c["id"]?>">
                <?php echo $c["name"]?>
            </a>
        </p>
        <p class="card-price">
            <?php echo $c["price"]?>€
        </p>
    </div>
<?php }?>

</div>

</div>
